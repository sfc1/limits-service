package com.sfc.microservices.limitsservice.bean;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class LimitConfiguration {
    private int minimum;
    private int maximum;
}
